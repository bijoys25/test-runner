package com.runner.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestRunnerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestRunnerApplication.class, args);
		System.out.println("Hello.");
	}

}
